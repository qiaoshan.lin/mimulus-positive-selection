#!/bin/bash
#SBATCH --job-name=chi2stat-M12
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=2G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o chi2stat-M12_%A_%a.out
#SBATCH -e chi2stat-M12_%A_%a.err

module load paml

for og in *mlc
do
	s=`grep 'lnL' $og|tail -2|awk '{print $5}'|awk 'NR == 1{null=$0} NR == 2 {s=($0-null)*2} END {print s}'`
	if (( $(echo "$s > 0" | bc -l) )); then
		echo $og
		chi2 2 $s
	fi
done


