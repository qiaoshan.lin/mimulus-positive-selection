#!/bin/bash
#SBATCH --job-name=codeml-M012
#SBATCH -a 0-556
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=2G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o codeml-M012_%A_%a.out
#SBATCH -e codeml-M012_%A_%a.err

module load paml

split -l 20 /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Results_Feb23/SingleCopyOrthogroups.txt singleOG
file=(singleOG*)
while read line; do   array+=($line);   done < ${file[$SLURM_ARRAY_TASK_ID]}
for og in ${array[@]}
do
	sed "s/orthogroup/$og/" codeml-M012_template.ctl > $og\-M012.ctl
	codeml $og\-M012.ctl
	rm $og\-M012.ctl
done


