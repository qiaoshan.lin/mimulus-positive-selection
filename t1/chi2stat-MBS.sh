#!/bin/bash
#SBATCH --job-name=chi2stat-MBS
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=2G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o chi2stat-MBS_%A_%a.out
#SBATCH -e chi2stat-MBS_%A_%a.err

module load paml

while read line; do   array+=($line);   done < /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Results_Feb23/SingleCopyOrthogroups.txt
for og in ${array[@]}
do
	s0=`grep 'lnL' codeml-MBS-H0_mp_Feb25/$og\-MBS-H0.mlc|awk '{print $5}'`
	s1=`grep 'lnL' codeml-MBS-H1_mp_Feb25/$og\-MBS-H1.mlc|awk '{print $5}'`
	s=`echo $s0 $s1|awk '$2-$1>0{print 2*($2-$1)}'`
	if [ "$s" != "" ]; then
#	if (( $(echo "$s > 0" | bc -l) )); then
		echo $og
		chi2 2 $s
	fi
done

# and then use awk 'NR%4{printf "%s ",$0;next;}1' chi2stat-MBS.txt |awk '$7<0.05{print $1}' | join -1 1 -2 1 - ~/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Results_Feb23/Orthogroups.csv to extract orthologs with p<0.05
