#!/bin/bash
#SBATCH --job-name=muscle
#SBATCH -a 0-556
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o muscle_%A_%a.out
#SBATCH -e muscle_%A_%a.err

module load muscle
module load seqkit
module load samtools

orthogroup=/home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Results_Feb23/Orthogroups.csv
split -l 20 /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Results_Feb23/SingleCopyOrthogroups.txt singleOG
file=(singleOG*)
while read line; do   array+=($line);   done < ${file[$SLURM_ARRAY_TASK_ID]}
for og in ${array[@]}
do
	mc=`grep $og $orthogroup | cut -f2`
	ml=`grep $og $orthogroup | cut -f3`
	mV=`grep $og $orthogroup | cut -f4`
	mg=`grep $og $orthogroup | cut -f5`
	mp=`grep $og $orthogroup | cut -f6 | sed -r 's/\s*$//'`

	samtools faidx /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/LF10_augustus.fasta $ml >> $og\.prot.fa
	samtools faidx /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/CE10_augustus.fasta $mc >> $og\.prot.fa
	samtools faidx /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/MVBL_augustus.fasta $mV >> $og\.prot.fa
	samtools faidx /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Mguttatus.fasta $mg >> $og\.prot.fa
	samtools faidx /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Mpar_augustus.fasta $mp >> $og\.prot.fa
	muscle -in $og\.prot.fa -out $og\.prot.aln

	samtools faidx ../LF10.cds.fa $ml >> $og\.nuc.fa
	samtools faidx ../CE10.cds.fa $mc >> $og\.nuc.fa 
	samtools faidx ../MVBL.cds.fa $mV >> $og\.nuc.fa 
	samtools faidx ../Mguttatus_256_v2.0.cds.fa $mg >> $og\.nuc.fa 
	samtools faidx ../Mpar.cds.fa $mp >> $og\.nuc.fa 

	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.prot.aln -o $og\.prot.sort.aln
	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.nuc.fa -o $og\.nuc.sort.fa

	perl ~/local/apps/pal2nal.v14/pal2nal.pl $og\.prot.sort.aln $og\.nuc.sort.fa -nogap -output paml > $og\.paml

	rm $og\.prot.fa $og\.nuc.fa $og\.prot.aln $og\.nuc.sort.fa $og\.prot.sort.aln
done
#cat *paml > all.paml.fa
#rm singleOG*


