#!/bin/bash
#SBATCH --job-name=codeml-MBS-H0
#SBATCH -a 0-556
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o codeml-MBS-H0_%A_%a.out
#SBATCH -e codeml-MBS-H0_%A_%a.err

module load paml

split -l 20 /home/CAM/qlin/LF10_Genome/11_BRAKER2/t3/orthoFinder/genomes/Results_Feb23/SingleCopyOrthogroups.txt singleOG
file=(singleOG*)
while read line; do   array+=($line);   done < ${file[$SLURM_ARRAY_TASK_ID]}
for og in ${array[@]}
do
	sed "s/orthogroup/$og/" codeml-MBS-H0_template.ctl > $og\-MBS-H0.ctl
	codeml $og\-MBS-H0.ctl
	rm $og\-MBS-H0.ctl
done


